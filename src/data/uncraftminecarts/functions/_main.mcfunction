from ./utils import MINECARTS, BOATS, UNCRAFTABLE, CONTENTS


# Uncrafting recipes
for item in UNCRAFTABLE:
    name = f"uncraftminecarts:uncraft_{item}"
    recipe name {
        "type": "minecraft:crafting_shapeless",
        "ingredients": [
            {
                "item": item
            }
        ],
        "result": {
            "item": "minecraft:knowledge_book"
        }
    }

    advancement name {
        "criteria": {
            "crafted": {
                "trigger": "minecraft:recipe_unlocked",
                "conditions": {
                    "recipe": name
                }
            }
        },
        "rewards": {
            "function": name
        }
    }

    function name:
        recipe take @s name
        advancement revoke @s only name
        clear @s minecraft:knowledge_book

        if item in MINECARTS:
            give @s minecart
        elif item in BOATS:
            give @s (item.replace("_chest", ""))
        
        give @s CONTENTS[item]

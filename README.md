# Uncraft Minecarts

A minecraft 1.20 data pack that lets you uncraft minecarts and boats.
- Put a minecart or boat with a utility block in them into a crafting table or crafting grid to convert it into a base minecart/boat and the block that was inside it.
- Will not work with custom minecarts/boats from data packs. It will simply uncraft the minecart into its vanilla form
- Command block minecarts are excluded